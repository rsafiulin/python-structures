"""
Check whether the input string is palindrome.
"""


def main():
    """Check palindrome."""
    s = input("Input word: ")
    if str(s) == str(s)[::-1]:
        print("Yes")
    else:
        print("No")



if __name__ == "__main__":
    main()
