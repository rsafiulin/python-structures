"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = int(input("Input number: "))
    d1 = n % 10
    n = n // 10
    d2 = n % 10
    d3 = n // 10
    print(d1+d2+d3)


if __name__ == "__main__":
    main()
